<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 20/04/19
 * Time: 23:38
 */



abstract class Connection
{
var $host    = "localhost";
var $usuario = "root";
var $senha = "";
var $banco = "scrum";	
private $mysqli;

public function Abrir() 
{	
  $this->mysqli = new mysqli($this->host, $this->usuario, $this->senha, $this->banco);
}
  
public function Fechar() 
{
  $this->mysqli->close();
}

public function getConnection()
{
    $configs = include(__DIR__.'/../../../config.php');
    $con =  mysqli_connect($configs['domain'], $configs['bd_user'], $configs['bd_password'], $configs['bd_database']);
    $con->set_charset("utf8");
    return $con;
}
}
?>
