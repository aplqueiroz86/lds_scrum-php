<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 28/04/19
 * Time: 07:49
 */

return array(
    //'document_root' => '/scrum-php',
    'document_root' => '/lds/lds_scrum-php',
    'domain' => 'localhost',
    'bd_user' => 'root',
    'bd_password'=> '',
    'bd_database' => 'scrum'
);